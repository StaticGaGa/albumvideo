//
//  AVMainViewController.m
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import "AVMainViewController.h"
#import "AVMakeViewController.h"

@interface AVMainViewController ()

@end

@implementation AVMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goToMakeVideo:(id)sender {
    AVMakeViewController *controller = [[AVMakeViewController alloc] init];
    [self presentViewController:controller animated:YES completion:NULL];
}

- (IBAction)goToMyVideo:(id)sender {
}

- (IBAction)goToTemplate:(id)sender {
}

- (IBAction)goToCamera:(id)sender {
}

- (IBAction)goToSettings:(id)sender {
}
@end
