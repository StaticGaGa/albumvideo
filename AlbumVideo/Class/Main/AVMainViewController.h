//
//  AVMainViewController.h
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AVMainViewController : UIViewController
- (IBAction)goToMakeVideo:(id)sender;
- (IBAction)goToMyVideo:(id)sender;
- (IBAction)goToTemplate:(id)sender;
- (IBAction)goToCamera:(id)sender;
- (IBAction)goToSettings:(id)sender;

@end
