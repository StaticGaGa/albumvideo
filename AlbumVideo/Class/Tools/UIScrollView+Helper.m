//
//  UIScrollView+Helper.m
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import "UIScrollView+Helper.h"
#import "UIView+Helper.h"

@implementation UIScrollView (Helper)

- (void)scrollToTopAnimated:(BOOL)animated {
    [self setContentOffset:CGPointMake(self.contentOffset.x, 0) animated:animated];
}

- (void)scrollToBottomAnimated:(BOOL)animated {
    CGFloat diff = self.contentSize.height - self.height;
    if (diff > 0) {
        [self setContentOffset:CGPointMake(self.contentOffset.x, diff) animated:animated];
    }
}


@end
