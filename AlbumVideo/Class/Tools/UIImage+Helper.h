//
//  UIImage+Helper.h
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Helper)
+ (UIImage *)imageNamedNoCache:(NSString *)name;
- (NSData *)pxdata;

- (UIImage *)zoomedImage:(CGFloat)zoomLevel
    interpolationQuality:(CGInterpolationQuality)quality;
- (UIImage *)thumbnailImage:(CGSize)centerSize
       interpolationQuality:(CGInterpolationQuality)quality;
- (UIImage *)croppedImage:(CGRect)bounds;
- (UIImage *)resizedImage:(CGSize)newSize
     interpolationQuality:(CGInterpolationQuality)quality;
- (UIImage *)resizedImageWithContentMode:(UIViewContentMode)contentMode
                                  bounds:(CGSize)bounds
                    interpolationQuality:(CGInterpolationQuality)quality;

@end
