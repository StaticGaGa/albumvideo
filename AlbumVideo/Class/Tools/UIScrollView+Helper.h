//
//  UIScrollView+Helper.h
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (Helper)
- (void)scrollToTopAnimated:(BOOL)animated;
- (void)scrollToBottomAnimated:(BOOL)animated;
@end
