//
//  ALAsset+Helper.h
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreLocation/CoreLocation.h>

@interface ALAsset (Helper)
@property (nonatomic) BOOL selected;
@property (nonatomic, getter = isCover) BOOL cover;

- (NSString *)urlString;
- (ALAssetOrientation)orientation;
- (UIImage *)smallImage;
- (UIImage *)fullResolutionImage;
- (UIImage *)fullScreenImage;
- (UIImage *)originalImage;
- (NSData *)rawData;
- (CLLocation *)location;
- (NSDate *)date;

@end
