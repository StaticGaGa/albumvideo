//
//  ALAsset+Helper.m
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import "ALAsset+Helper.h"
#import <objc/runtime.h>
#import <ImageIO/ImageIO.h>

static char kAssociatedSelectionKey;
static char kAssociatedCoverKey;

@implementation ALAsset (Helper)

- (BOOL)selected {
    id obj = objc_getAssociatedObject (self, &kAssociatedSelectionKey);
    return [obj boolValue];
}

- (void)setSelected:(BOOL)selected {
    
    objc_setAssociatedObject (self,
                              &kAssociatedSelectionKey,
                              [NSNumber numberWithBool:selected],
                              OBJC_ASSOCIATION_RETAIN
                              );
#if 0
    dispatch_async([PaiXiu dispatchQueueSelectInLocalAlbum], ^{
        //    dispatch_async(DUGetGlobalQueue(), ^{
        @autoreleasepool {
            if (selected) {
                [self saveImageDataForUploading];
                dispatch_async(DUGetGlobalQueue(), ^{
                    [self fetchCamera];
                });
            }else {
                [self clearImageDataForUploading];
            }
        }
    });
#endif
}

- (BOOL)isCover {
    id obj = objc_getAssociatedObject (self, &kAssociatedCoverKey);
    return [obj boolValue];
}

- (void)setCover:(BOOL)cover {
    objc_setAssociatedObject (self,
                              &kAssociatedCoverKey,
                              [NSNumber numberWithBool:cover],
                              OBJC_ASSOCIATION_RETAIN
                              );
}

- (NSString *)urlString {
    NSArray *urls = [[self valueForProperty:ALAssetPropertyURLs] allValues];
    if (0 == urls.count) {
        return nil;
    }
    return [[urls objectAtIndex:0] absoluteString];
}

- (ALAssetOrientation)orientation {
    return [[self valueForProperty:ALAssetPropertyOrientation] integerValue];
}

- (UIImage *)smallImage {
    return [UIImage imageWithCGImage:[self thumbnail]];
}

+ (BOOL)systemVersionLessThan:(NSString *)version {
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    return NSOrderedAscending == [systemVersion compare:version options:NSNumericSearch];
}

- (UIImage *)fullResolutionImage {
    return [UIImage imageWithCGImage:self.defaultRepresentation.fullResolutionImage scale:1 orientation:self.orientation];
}

- (UIImage *)fullScreenImage {

    
    return [ALAsset systemVersionLessThan:@"5.0"]? [UIImage imageWithCGImage:self.defaultRepresentation.fullScreenImage scale:1 orientation:self.orientation]: [UIImage imageWithCGImage:self.defaultRepresentation.fullScreenImage];
}

- (UIImage *)originalImage {
    return [UIImage imageWithData:self.rawData];
}


- (NSData *)rawData {
    long long size = self.defaultRepresentation.size;
    if (self.defaultRepresentation.size > 1024 * 1024 * 25) {
        return nil;
    }
    
    uint8_t *buffer = malloc(size);
    NSError *error = nil;
    NSUInteger writtenBytes = [self.defaultRepresentation getBytes:buffer fromOffset:0 length:size error:&error];
    if (size != writtenBytes) {
        free(buffer);
        return nil;
    }
    
    return [NSData dataWithBytesNoCopy:buffer length:size freeWhenDone:YES];
}

- (CLLocation *)location {
    return [self valueForProperty:ALAssetPropertyLocation];
}

- (NSDate *)date {
    return [self valueForProperty:ALAssetPropertyDate];
}

- (NSString *)localURL {
    if (self.urlString.length > 0) {
        return self.urlString;
    }else {
        return @"";
    }
}

@end
