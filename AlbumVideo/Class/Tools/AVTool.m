//
//  AVTool.m
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import "AVTool.h"
#import "ALAsset+Helper.h"
#import <AVFoundation/AVFoundation.h>

@implementation AVTool
+ (ALAssetsLibrary *)assetsLibrary {
    static ALAssetsLibrary *sharedLibrary = NULL;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedLibrary = [[ALAssetsLibrary alloc] init];
    });
    return sharedLibrary;
}

#pragma mark -
#pragma mark - merge && save

- (void)tt {
    AVAsset *asset;
    CGSize videoSize;
    AVAssetTrack *assetVideoTrack = nil;
	AVAssetTrack *assetAudioTrack = nil;
	// Check if the asset contains video and audio tracks
	if ([[asset tracksWithMediaType:AVMediaTypeVideo] count] != 0) {
		assetVideoTrack = [asset tracksWithMediaType:AVMediaTypeVideo][0];
	}
	if ([[asset tracksWithMediaType:AVMediaTypeAudio] count] != 0) {
		assetAudioTrack = [asset tracksWithMediaType:AVMediaTypeAudio][0];
	}
    
    CMTime insertionPoint = kCMTimeZero;
	NSError *error = nil;
    AVMutableComposition *mutableComposition = [AVMutableComposition composition];
    // Insert the video and audio tracks from AVAsset
    if (nil != assetVideoTrack) {
        AVMutableCompositionTrack *compositionVideoTrack = [mutableComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
        [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, [asset duration]) ofTrack:assetVideoTrack atTime:insertionPoint error:&error];
    }
    
    if (assetAudioTrack != nil) {
        AVMutableCompositionTrack *compositionAudioTrack = [mutableComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
        [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, [asset duration]) ofTrack:assetAudioTrack atTime:insertionPoint error:&error];
    }
    
	// Step 2
	// Create a water mark layer of the same size as that of a video frame from the asset
    if ([[mutableComposition tracksWithMediaType:AVMediaTypeVideo] count] != 0) {
        AVMutableVideoComposition *mutableVideoComposition = [AVMutableVideoComposition videoComposition];
        mutableVideoComposition.frameDuration = CMTimeMake(1, 30); // 30 fps
        mutableVideoComposition.renderSize = assetVideoTrack.naturalSize;
    
        AVMutableVideoCompositionInstruction *passThroughInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
        passThroughInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, [mutableComposition duration]);
        
        AVAssetTrack *videoTrack = [mutableComposition tracksWithMediaType:AVMediaTypeVideo][0];
        AVMutableVideoCompositionLayerInstruction *passThroughLayer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
        
        passThroughInstruction.layerInstructions = @[passThroughLayer];
        mutableVideoComposition.instructions = @[passThroughInstruction];
        
        videoSize = mutableVideoComposition.renderSize;

        
        CALayer *animationLayer = [CALayer layer];
        animationLayer.bounds = CGRectMake(0, 0, assetVideoTrack.naturalSize.width, assetVideoTrack.naturalSize.height);
        CATextLayer *titleLayer = [CATextLayer layer];
        titleLayer.string = @"GaGaGa";
        titleLayer.fontSize = 30;
        
        titleLayer.alignmentMode = kCAAlignmentCenter;
        titleLayer.bounds = CGRectMake(0, 0, videoSize.width, videoSize.height / 6);
        
        [animationLayer addSublayer:titleLayer];
        titleLayer.anchorPoint =  CGPointMake(0.5, 0.5);
        titleLayer.position = CGPointMake(CGRectGetMidX(animationLayer.bounds), CGRectGetMidY(animationLayer.bounds));
        
        CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        fadeAnimation.fromValue = [NSNumber numberWithFloat:1.0];
        fadeAnimation.toValue = [NSNumber numberWithFloat:0.0];
        fadeAnimation.additive = NO;
        fadeAnimation.removedOnCompletion = NO;
        fadeAnimation.beginTime = 3.5;
        fadeAnimation.duration = 1.0;
        fadeAnimation.repeatCount = 10000;
        fadeAnimation.fillMode = kCAFillModeBoth;
        [titleLayer addAnimation:fadeAnimation forKey:nil];

        CALayer *parentLayer = [CALayer layer];
        CALayer *videoLayer = [CALayer layer];
        
        parentLayer.bounds = CGRectMake(0, 0, videoSize.width, videoSize.height);
        parentLayer.anchorPoint =  CGPointMake(0, 0);
        parentLayer.position = CGPointMake(0, 0);
        
        videoLayer.bounds = CGRectMake(0, 0, videoSize.width, videoSize.height);
        [parentLayer addSublayer:videoLayer];
        videoLayer.anchorPoint =  CGPointMake(0.5, 0.5);
        videoLayer.position = CGPointMake(CGRectGetMidX(parentLayer.bounds), CGRectGetMidY(parentLayer.bounds));
        [parentLayer addSublayer:animationLayer];
        animationLayer.anchorPoint =  CGPointMake(0.5, 0.5);
        animationLayer.position = CGPointMake(CGRectGetMidX(parentLayer.bounds), CGRectGetMidY(parentLayer.bounds));
        
        mutableVideoComposition.animationTool = [AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];

    }

}

+ (void)mergeVideo:(NSArray *)videosArray {
    if (!videosArray || 0 == videosArray.count) {
        return;
    }
    
    AVMutableComposition* mixComposition = [[AVMutableComposition alloc] init];
    
    CMTime totalDuration = kCMTimeZero;
    
    AVMutableVideoCompositionInstruction * MainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    NSMutableArray *layerInstructionArray = [NSMutableArray array];
    for (ALAsset *asset in videosArray) {
        NSURL *url = [[asset defaultRepresentation] url];
        AVAsset *avAsset = [AVAsset assetWithURL:url];
        if (avAsset) {
            AVMutableCompositionTrack *track = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
            [track insertTimeRange:CMTimeRangeMake(kCMTimeZero, avAsset.duration) ofTrack:[[avAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:totalDuration error:nil];
                        
            AVMutableVideoCompositionLayerInstruction *layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:track];
            AVAssetTrack *assetTrack = [[avAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
            
            UIImageOrientation assetOrientation_  = UIImageOrientationUp;
            BOOL  isFirstAssetPortrait_  = NO;
            CGAffineTransform firstTransform = assetTrack.preferredTransform;
            if(firstTransform.a == 0 && firstTransform.b == 1.0 && firstTransform.c == -1.0 && firstTransform.d == 0)  {assetOrientation_= UIImageOrientationRight; isFirstAssetPortrait_ = YES;}
            if(firstTransform.a == 0 && firstTransform.b == -1.0 && firstTransform.c == 1.0 && firstTransform.d == 0)  {assetOrientation_ =  UIImageOrientationLeft; isFirstAssetPortrait_ = YES;}
            if(firstTransform.a == 1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == 1.0)   {assetOrientation_ =  UIImageOrientationUp;}
            if(firstTransform.a == -1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == -1.0) {assetOrientation_ = UIImageOrientationDown;}

            CGFloat FirstAssetScaleToFitRatio = 320.0/assetTrack.naturalSize.width;

            if(isFirstAssetPortrait_){
                NSLog(@"isFirstAsset %i",isFirstAssetPortrait_);
                        FirstAssetScaleToFitRatio = 320.0/assetTrack.naturalSize.height;
                            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
                [layerInstruction setTransform:CGAffineTransformConcat(assetTrack.preferredTransform, FirstAssetScaleFactor) atTime:totalDuration];

            }else{
                NSLog(@"isFirstAsset %i",isFirstAssetPortrait_);
                CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
                [layerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(assetTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(0, 160)) atTime:totalDuration];
            }

            totalDuration = CMTimeAdd(totalDuration, avAsset.duration);

            [layerInstruction setOpacity:0.0 atTime:totalDuration];
            [layerInstructionArray addObject:layerInstruction];
            
        }
    }
    MainInstruction.layerInstructions = layerInstructionArray;
    MainInstruction.backgroundColor = [[UIColor clearColor] CGColor];
    
    MainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, totalDuration);
    
    AVMutableVideoComposition *MainCompositionInst = [AVMutableVideoComposition videoComposition];
    MainCompositionInst.instructions = [NSArray arrayWithObject:MainInstruction];
    MainCompositionInst.frameDuration = CMTimeMake(1, 30);
    MainCompositionInst.renderSize = CGSizeMake(320.0, 480.0);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setAMSymbol:@"AM"];
    [dateFormatter setPMSymbol:@"PM"];
    [dateFormatter setDateFormat:@"dd-MM-yyyy-hh:mm:ssaaa"];
    NSDate *date = [NSDate date];
    NSString *moveName = [dateFormatter stringFromDate:date];
    
    NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-%@.mov",myGroupName,moveName]];
    
    NSURL *url = [NSURL fileURLWithPath:myPathDocs];
    NSLog(@"url %@",url);
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    
    exporter.outputURL=url;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    exporter.videoComposition = MainCompositionInst;
    exporter.shouldOptimizeForNetworkUse = YES;
    [exporter exportAsynchronouslyWithCompletionHandler:^
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             [AVTool exportDidFinish:exporter];
         });
     }];
}

+ (void)exportDidFinish:(AVAssetExportSession*)session
{
    NSLog(@"status %i",session.status);
    NSLog(@"error %@ %@",session.error,session.error.localizedFailureReason);
    
    if(session.status == AVAssetExportSessionStatusCompleted){
        NSURL *outputURL = session.outputURL;
        ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
        if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
            [library writeVideoAtPathToSavedPhotosAlbum:outputURL
                                        completionBlock:^(NSURL *assetURL, NSError *error){
                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                if (error) {
                                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Saving Failed"  delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil, nil];
                                                    [alert show];
                                                }else{
                                                    
                                                    [AVTool addAssetWithURL:assetURL toPhotoAlbum:myGroupName withFinishedBlock:^(BOOL result) {
                                                        NSLog(@"result %i",result);
                                                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Saved" message:@"Saved To Photo Album"  delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                                                        [alert show];

                                                    }];
                                                    
                                                }
                                                
                                            });
                                            
                                        }];
        }
    }
}

+ (void) addAssetWithURL:(NSURL *) assetURL toPhotoAlbum:(NSString *) album withFinishedBlock:(AddAssetToGroupBlock)finishedBlock
{
    __block ALAssetsLibrary *library = [AVTool assetsLibrary];
    
    [library assetForURL:assetURL
             resultBlock:^(ALAsset *asset)
     {
         NSString *groupName = [[NSUserDefaults standardUserDefaults] objectForKey:@"groupURL"];
         NSURL *groupURL = [[NSURL alloc] initWithString:groupName?groupName:@""];
         
         [library groupForURL:groupURL
                  resultBlock:^(ALAssetsGroup *group)
          {
              NSString *groupName = [group valueForProperty:ALAssetsGroupPropertyName];
              
              if ([album isEqualToString:groupName])
              {
                  NSLog(@"addAsset %@",album);
                  [group addAsset:asset];
                  finishedBlock(YES);
                  return ;
              }
              else
              {
                  __weak ALAssetsLibrary *lib = library;
                  
                  [library addAssetsGroupAlbumWithName:album resultBlock:^(ALAssetsGroup *group)
                   {
                       NSString *groupName = [group valueForProperty:ALAssetsGroupPropertyName];
                       NSURL *groupURL = [group valueForProperty:ALAssetsGroupPropertyURL];
                       [[NSUserDefaults standardUserDefaults] setObject:groupURL.absoluteString forKey:@"groupURL"];
                       
                       [lib enumerateGroupsWithTypes:ALAssetsGroupAlbum usingBlock:^(ALAssetsGroup *group, BOOL *stop)
                        {
                            if ([album isEqualToString:groupName])
                            {
                                NSLog(@"addAsset");
                                [group addAsset:asset];
                                *stop = YES;
                                finishedBlock(YES);
                                return ;
                            }
                            
                        } failureBlock:^(NSError *error)
                        {
                            finishedBlock(NO);
                        }];
                       
                   } failureBlock:^(NSError *error)
                   {
                       finishedBlock(NO);
                   }];
              }
              
          } failureBlock:^(NSError *error)
          {
              finishedBlock(NO);
          }];
         
     } failureBlock:^(NSError *error)
     {
         finishedBlock(NO);
     }];
}

#pragma mark -
#pragma mark - add waterMark to video

+ (void)addWatermark:(AVAsset *)asset {
    if (!asset) {
        return;
    }
    
    CALayer *animationLayer = [CALayer layer];
    CGSize videoSize = CGSizeMake(320, 480);
    animationLayer.bounds = CGRectMake(0, 0, 320, 480);

    CATextLayer *titleLayer = [CATextLayer layer];
    titleLayer.string = @"GaGaGa===";
    titleLayer.fontSize = 30;
    
    titleLayer.alignmentMode = kCAAlignmentCenter;
    titleLayer.bounds = CGRectMake(0, 0, videoSize.width, videoSize.height / 6);
    
    [animationLayer addSublayer:titleLayer];
    titleLayer.anchorPoint =  CGPointMake(0.5, 0.5);
    titleLayer.position = CGPointMake(CGRectGetMidX(animationLayer.bounds), CGRectGetMidY(animationLayer.bounds));
    
    
    CABasicAnimation *fadeAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeAnimation.fromValue = [NSNumber numberWithFloat:1.0];
    fadeAnimation.toValue = [NSNumber numberWithFloat:0.0];
    fadeAnimation.additive = NO;
    fadeAnimation.removedOnCompletion = NO;
    fadeAnimation.beginTime = 3.5;
    fadeAnimation.duration = 1.0;
    fadeAnimation.repeatCount = 10000;
    fadeAnimation.fillMode = kCAFillModeBoth;
    [titleLayer addAnimation:fadeAnimation forKey:nil];
       
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    
    parentLayer.bounds = CGRectMake(0, 0, videoSize.width, videoSize.height);
    parentLayer.anchorPoint =  CGPointMake(0, 0);
    parentLayer.position = CGPointMake(0, 0);
    
    videoLayer.bounds = CGRectMake(0, 0, videoSize.width, videoSize.height);
    [parentLayer addSublayer:videoLayer];
    videoLayer.anchorPoint =  CGPointMake(0.5, 0.5);
    videoLayer.position = CGPointMake(CGRectGetMidX(parentLayer.bounds), CGRectGetMidY(parentLayer.bounds));
    [parentLayer addSublayer:animationLayer];
    animationLayer.anchorPoint =  CGPointMake(0.5, 0.5);
    animationLayer.position = CGPointMake(CGRectGetMidX(parentLayer.bounds), CGRectGetMidY(parentLayer.bounds));
    

}
@end
