//
//  AVTool.h
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
@class AVAsset;

#define myGroupName @"动感影集"

typedef void (^AddAssetToGroupBlock)(BOOL result);


@interface AVTool : NSObject

+ (ALAssetsLibrary *)assetsLibrary;
+ (void)mergeVideo:(NSArray *)videosArray;
+ (void)addWatermark:(AVAsset *)asset;
@end
