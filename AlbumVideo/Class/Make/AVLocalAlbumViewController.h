//
//  AVLocalAlbumViewController.h
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALAsset+Helper.h"

typedef enum {
    kFilterAllPhotos,
    kFilterAllVideos,
}kFilterType;

typedef enum {
    kUsedForMerge,
    kUsedForWaterMark,
}kUsedFor;
@interface AVLocalAlbumViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *bottomScrollView;
@property (weak, nonatomic) IBOutlet UITableView *albumTableView;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
- (IBAction)done:(id)sender;

- (id)initWithFilterType:(kFilterType)type usedFor:(kUsedFor)usedType;
- (IBAction)cancel:(id)sender;
- (void)selectAsset:(ALAsset *)asset;
- (void)unselectAsset:(ALAsset *)asset;
@end
