//
//  AVMakeViewController.h
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AVMakeViewController : UIViewController
- (IBAction)makeAppendVideo:(id)sender;
- (IBAction)makeWatermarkVideo:(id)sender;
- (IBAction)makePhotoVideo:(id)sender;
- (IBAction)makeSoundVideo:(id)sender;
- (IBAction)goBack:(id)sender;

@end
