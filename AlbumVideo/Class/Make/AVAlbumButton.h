//
//  AVAlbumButton.h
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AVAlbumButton : UIButton
{
}

@property (nonatomic, strong) id object;

- (void)setButtonCoverView:(UIImageView *)coverView;
- (void)showCoverView;
- (void)hideCoverView;
@end
