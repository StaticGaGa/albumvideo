//
//  AVMakeViewController.m
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import "AVMakeViewController.h"
#import "AVLocalAlbumViewController.h"
#import "AVAddMarkViewController.h"

@interface AVMakeViewController ()

@end

@implementation AVMakeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)makeAppendVideo:(id)sender {
    AVLocalAlbumViewController *albumController = [[AVLocalAlbumViewController alloc] initWithFilterType:kFilterAllVideos usedFor:kUsedForMerge];
    [self presentViewController:albumController animated:YES completion:NULL];
}

- (IBAction)makeWatermarkVideo:(id)sender {
    AVLocalAlbumViewController *albumController = [[AVLocalAlbumViewController alloc] initWithFilterType:kFilterAllVideos usedFor:kUsedForWaterMark];
    [self presentViewController:albumController animated:YES completion:NULL];

}

- (IBAction)makePhotoVideo:(id)sender {
}

- (IBAction)makeSoundVideo:(id)sender {
}

- (IBAction)goBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}
@end
