//
//  AVAlbumCell.m
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import "AVAlbumCell.h"
#import "UIView+Helper.h"
#import "AVAlbumButton.h"
#import "ALAsset+Helper.h"
#import "AVLocalAlbumViewController.h"

const CGFloat kLAPhotoDimension = 100;
//const NSUInteger kLAPhotoColumns = 3;
const CGFloat kLAPhotoSpacing = (320 - 3 * kLAPhotoDimension) / (3 + 1);

@interface AVAlbumCell ()

@end

@implementation AVAlbumCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        CGFloat x = kLAPhotoSpacing;

        for (NSUInteger i = 0; i < 3; ++i) {
            AVAlbumButton *button = [AVAlbumButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(x, kLAPhotoSpacing, kLAPhotoDimension, kLAPhotoDimension);
            [button addTarget:self action:@selector(toggleAssetSelection:) forControlEvents:UIControlEventTouchUpInside];
            
            UIImageView *coverView = [[UIImageView alloc] initWithFrame:button.bounds];
            coverView.image = [UIImage imageNamed:@"btn_cover_white"];
            UIImageView *selectMark = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ico_check_photo_selected"]];
            selectMark.right = coverView.width;
            selectMark.bottom = coverView.height;
            [coverView addSubview:selectMark];
            [button setButtonCoverView:coverView];
            [button hideCoverView];
            [self.contentView addSubview:button];
            x += kLAPhotoDimension + kLAPhotoSpacing;
        }

    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellWithAssets:(NSArray *)assets {
    NSArray *imageViews = self.contentView.subviews;
    for (NSUInteger i = 0; i < imageViews.count; ++i) {
        AVAlbumButton *imageView = [imageViews objectAtIndex:i];
        imageView.hidden = YES;
        if (i < assets.count) {
            imageView.hidden = NO;
            ALAsset *asset = [assets objectAtIndex:i];
            imageView.object = asset;
            [imageView setImage:asset.smallImage forState:UIControlStateNormal];
            
            if (asset.selected) {
                [imageView showCoverView];
            }else {
                [imageView hideCoverView];
            }
        }
    }
}

- (void)toggleAssetSelection:(id)sender {
    AVAlbumButton *imageView = (AVAlbumButton *)sender;
    ALAsset *asset = imageView.object;
    asset.selected = !asset.selected;
    AVLocalAlbumViewController *vc = (AVLocalAlbumViewController *)self.viewController;
    if (asset.selected) {
        [imageView showCoverView];
        [vc selectAsset:asset];
    }else {
        [imageView hideCoverView];
        [vc unselectAsset:asset];
    }
}
@end
