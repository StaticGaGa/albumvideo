//
//  AVAlbumButton.m
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import "AVAlbumButton.h"

@interface AVAlbumButton ()
@property (nonatomic, strong) UIImageView *m_coverView;
@end

@implementation AVAlbumButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setButtonCoverView:(UIImageView *)coverView {
    self.m_coverView = coverView;
    [self addSubview:self.m_coverView];
}
- (void)showCoverView {
    if (self.m_coverView) {
        self.m_coverView.hidden = NO;
    }
}
- (void)hideCoverView {
    if (self.m_coverView) {
        self.m_coverView.hidden = YES;
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
