//
//  AVAlbumCell.h
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AVAlbumCell : UITableViewCell
- (void)setCellWithAssets:(NSArray *)assets;
@end

