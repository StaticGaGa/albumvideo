//
//  AVLocalAlbumViewController.m
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import "AVLocalAlbumViewController.h"
#import "UIImage+Helper.h"
#import "AVTool.h"
#import "UIScrollView+Helper.h"
#import "AVAlbumCell.h"
#import "UIView+Helper.h"
#import "AVAlbumButton.h"
#import "AVAddMarkViewController.h"

extern const CGFloat kLAPhotoDimension;
extern const NSUInteger kLAPhotoColumns;
extern const CGFloat kLAPhotoSpacing;

const NSUInteger kLAPhotoColumns = 3;

@interface AVLocalAlbumViewController ()
@property (nonatomic, assign) BOOL firstDisplayAtBottom;
@property (nonatomic, strong) NSMutableArray *assets;
@property (nonatomic, strong) NSMutableArray *selectedAssets;
@property (nonatomic, assign) kFilterType m_filter;
@property (nonatomic, assign) kUsedFor m_usedFor;
@end

@implementation AVLocalAlbumViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.assets = [[NSMutableArray alloc] init];
        self.selectedAssets = [[NSMutableArray alloc] init];
        self.firstDisplayAtBottom = YES;
    }
    return self;
}


- (IBAction)done:(id)sender {
    switch (self.m_usedFor) {
        case kUsedForMerge:
        {
            [AVTool mergeVideo:self.selectedAssets];
        }
            break;
        case kUsedForWaterMark:
        {
            if (self.selectedAssets.count > 0) {
            AVAsset *asset = [self.selectedAssets objectAtIndex:0];
            AVAddMarkViewController *markController = [[AVAddMarkViewController alloc] initWithAsset:asset];
            [self presentViewController:markController animated:YES completion:NULL];
            }
        }
            break;
        default:
            break;
    }
}

- (id)initWithFilterType:(kFilterType)type usedFor:(kUsedFor)usedType{
    if (self = [super init]) {
        self.m_filter = type;
        self.m_usedFor = usedType;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.albumTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self enumerateAssetsGroupWithType:ALAssetsGroupSavedPhotos];
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - tableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    //    PXLog(@"tableView %@ numberOfRowsInSection %u", tableView, section);
    return (self.assets.count + kLAPhotoColumns - 1) / kLAPhotoColumns;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    PXLog(@"tableView %@ cellForRowAtIndexPath %@", tableView, indexPath);
    
    static NSString *CellIdentifier = @"LocalAlbumCell";
    
    AVAlbumCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(!cell){
        cell = [[AVAlbumCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    NSUInteger location = indexPath.row * kLAPhotoColumns;
    NSUInteger diff = self.assets.count - location;
    NSUInteger length = diff > kLAPhotoColumns? kLAPhotoColumns: diff;
    [cell setCellWithAssets:[self.assets subarrayWithRange:NSMakeRange(location, length)]];
    
    return cell;
}

#pragma mark - delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kLAPhotoDimension + kLAPhotoSpacing;
}

#pragma mark - private

- (void)enumerateAssetsGroupWithType:(ALAssetsGroupType)groupType {
    [[AVTool assetsLibrary] enumerateGroupsWithTypes:groupType usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (!group) {
            return;
        }
        
        [self enumerateAssetsWithGroup:group];
        *stop = YES;
    } failureBlock:^(NSError *error) {
        NSLog(@"enumerateAssetsGroupWithType error %@", error);
        [self showErrorView];
    }];
}

- (void)showErrorView {

    UIView *superview = self.albumTableView;
    
    UILabel *label1 = [[UILabel alloc] init];
    label1.backgroundColor = [UIColor clearColor];
    label1.text = @"无法访问相册照片";
    label1.font = [UIFont systemFontOfSize:16];
    label1.textColor = [UIColor grayColor];
    [label1 sizeToFit];
    label1.centerX = superview.width / 2;
    label1.top = 120;
    [superview addSubview:label1];
    
    UILabel *label2 = [[UILabel alloc] init];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = @"由于iPhone系统限制，需要您";
    label2.font = [UIFont systemFontOfSize:14];
    label2.textColor = [UIColor grayColor];
    [label2 sizeToFit];
    label2.centerX = superview.width / 2;
    label2.top = label1.bottom + 20;
    [superview addSubview:label2];
    
    UILabel *label3 = [[UILabel alloc] init];
    label3.backgroundColor = [UIColor clearColor];
    NSString *info = nil;
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0) {
        info = @"在设置 - 隐私的照片内打开对拍秀的访问允许";
    }else {
        info = @"在设置内打开拍秀的定位服务才可正常访问";
    }
    label3.text = info;
    label3.font = [UIFont systemFontOfSize:14];
    label3.textColor = [UIColor grayColor];
    [label3 sizeToFit];
    label3.centerX = superview.width / 2;
    label3.top = label2.bottom + 6;
    [superview addSubview:label3];
}


- (void)enumerateAssetsWithGroup:(ALAssetsGroup *)group {
    BOOL scrollToBottom = NO;
    if (self.firstDisplayAtBottom) {
        scrollToBottom = 0 == self.assets.count? YES: NO;
    }
    
    NSUInteger groupType = [[group valueForProperty:ALAssetsGroupPropertyType] unsignedIntegerValue];
    NSString *groupName = [group valueForProperty:ALAssetsGroupPropertyName];
    
    [[AVTool assetsLibrary] enumerateGroupsWithTypes:groupType usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        
        if (!group) {
            return;
        }
        
        NSString *currentGroupName = [group valueForProperty:ALAssetsGroupPropertyName];
        if (groupName && ![groupName isEqualToString:currentGroupName]) {
            return;
        }
        NSString *footStr = @"";
        switch (self.m_filter) {
            case kFilterAllPhotos:
            {
                footStr = @"张照片";
                [group setAssetsFilter:[ALAssetsFilter allPhotos]];
            }
                break;
            case kFilterAllVideos:
            {
                footStr = @"个视频";
                [group setAssetsFilter:[ALAssetsFilter allVideos]];
            }
                break;
            default:
                break;
        }

        NSMutableArray *assets = [NSMutableArray array];
        [group enumerateAssetsUsingBlock:^(ALAsset *asset, NSUInteger index, BOOL *innerStop) {
            @autoreleasepool {
                if (!asset) {
                    *innerStop = YES;
                    return;
                }
                [assets addObject:asset];
            }
        }];
        
        [self.assets setArray:assets];
        UILabel *footer = (UILabel *)self.albumTableView.tableFooterView;
        if (self.assets.count > 0) {
            footer.text = [NSString stringWithFormat:@"%u%@", self.assets.count,footStr];
        }else {
            footer.text = @"";
        }
        [self.albumTableView reloadData];
        
        if (scrollToBottom && self.assets.count > 0) {
            [self.albumTableView scrollToBottomAnimated:NO];
        }
        *stop = YES;
    } failureBlock:^(NSError *error) {
        NSLog(@"enum assets group failed %@", error);
    }];
}

- (void)updateBottomScrollView {
//    NSArray *selectedAssets = [self selectedAssets];
    NSArray *selectedAssets = self.selectedAssets;
    NSUInteger imageCount = 0;
    
    CGFloat centerX = 10;
    CGFloat width = 0;
    for (UIView *view in self.bottomScrollView.subviews) {
        [view removeFromSuperview];
    }
    [self.bottomScrollView setContentSize:CGSizeMake(0, 0)];

    for (ALAsset *selectedAsset in selectedAssets) {
        AVAlbumButton *button = [AVAlbumButton buttonWithType:UIButtonTypeCustom];
        UIImage *image = selectedAsset.smallImage;
        [button setImage:image forState:UIControlStateNormal];
        imageCount++;
        button.size = CGSizeMake(70, 70);
        button.origin = CGPointMake(centerX, 10);
        [self.bottomScrollView addSubview:button];
        width = button.width;
        centerX += width + 10;
    }
    [self.bottomScrollView setContentSize:CGSizeMake((width + 10)*imageCount, 0)];
    
}

#pragma mark -
#pragma mark - public
- (void)selectAsset:(ALAsset *)asset {
    [self.selectedAssets addObject:asset];
    [self updateBottomScrollView];
}
- (void)unselectAsset:(ALAsset *)asset {
    if ([self.selectedAssets containsObject:asset]) {
        [self.selectedAssets removeObject:asset];
    }
    [self updateBottomScrollView];
}


@end
