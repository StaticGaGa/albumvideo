//
//  AppDelegate.h
//  AlbumVideo
//
//  Created by Static Ga on 13-6-1.
//  Copyright (c) 2013年 Static Ga. All rights reserved.
//

#import <UIKit/UIKit.h>
@class AVMainViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    AVMainViewController *mainController;
}
@property (strong, nonatomic) UIWindow *window;

@end
